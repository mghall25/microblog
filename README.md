<h1>Welcome to Microblog!</h1>
This was created using Miguel Grinberg's 
<a href="https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world" rel="nofollow">Flask Mega-Tutorial</a>
as a way to learn how an app is built from the ground up.